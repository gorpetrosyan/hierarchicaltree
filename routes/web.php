<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['web'])->group(function () {
    Route::get('/', 'CategoryController@index')->name('index');
    Route::post('/create', 'CategoryController@store')->name('store');
    Route::get('/show/{id}', 'CategoryController@show')->name('show');
    Route::put('/update/{id}', 'CategoryController@edit')->name('edit');
    Route::delete('/delete/{id}', 'CategoryController@remove')->name('remove');
});
