<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return bool|void|null
     */

    /**
     * @var array
     */
    protected $casts = [
        'parent_id' => 'integer'
    ];

    public function delete()
    {
        DB::transaction(function () {

            parent::boot();

            static::deleting(function($cat) {
                foreach($cat->subCategories as $c){
                    $c->delete();
                }
            });
            parent::delete();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }
}
