<?php


namespace App\Repository\Eloquent;


use App\Category;
use App\Repository\CategoryRepositoryInterface;
use Illuminate\Support\Collection;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{

    /**
     * CategoryRepository constructor.
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return ['all'=>$this->model->orderBy('title','ASC')->get(), 'main'=>$this->model->where('parent_id',0)->orderBy('title','ASC')->get()];
    }
}
