<?php


namespace App\Repository\Eloquent;
use App\Repository\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function store(array $attributes): Model
    {
        return $this->model->create($attributes);
    }


    /**
     * @param array $attributes
     * @param $id
     * @return Model
     */
    public function edit(array $attributes,$id): Model
    {
         $model =  $this->model->findOrFail($id);
         $model->update($attributes);
         return $model;
    }

    /**
     * @param $id
     * @return Model
     */
    public function show($id) : Model
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $id
     * @return Model
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}
