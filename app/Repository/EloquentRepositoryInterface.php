<?php


namespace App\Repository;
use Illuminate\Database\Eloquent\Model;
use App\Category;


interface EloquentRepositoryInterface
{

    /**
     * @param array $attributes
     * @return Model
     */
    public function store(array $attributes): Model;

    /**
     * @param array $attributes
     * @param $id
     * @return Model
     */
    public function edit(array $attributes ,$id): Model;

    /**
     * @param $id
     * @return Model
     */
    public function show($id) : Model;

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
