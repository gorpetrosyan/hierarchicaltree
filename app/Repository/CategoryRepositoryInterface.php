<?php

namespace App\Repository;


interface CategoryRepositoryInterface
{
    public function all(): array;

}
