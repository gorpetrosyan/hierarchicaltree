<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\EloquentRepositoryInterface;

class CategoryController extends Controller
{
    /**
     * @var
     */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('welcome', ['all' => $categories['all'], 'main' => $categories['main']]);
    }

    /**
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        $this->categoryRepository->store($request->all());
        return redirect()->back()->with('success', 'Category was successfully created');
    }

    /**
     * @param CategoryRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(CategoryRequest $request, $id)
    {
        $this->categoryRepository->edit($request->all(), $id);
        return response()->json('Category was successfully updated', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $category = $this->categoryRepository->show($id);
        return view('show', compact('category'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id)
    {
        $this->categoryRepository->delete($id);
        return response()->json('Category was successfully deleted', 200);
    }
}
