export default function (header, body , isSuccess) {
    const headerTag = document.getElementById('toasterHeader');
    const bodyTag = document.getElementById('toasterBody');
    headerTag.innerText = header;
    bodyTag.innerText = body;
       if(isSuccess){
           headerTag.style.color = 'green';
           bodyTag.style.color = 'green' ;
       }else{
           headerTag.style.color = 'red';
           bodyTag.style.color = 'red' ;
       }
       const options = {
           delay : 3000
       };
    $('#toasterInit').toast(options);
    $('#toasterInit').toast('show');
}
