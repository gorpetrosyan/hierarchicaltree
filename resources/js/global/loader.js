export function loading(arg) {
    const loader = document.getElementById('loading-container');
    arg ? loader.style.display = 'block' : loader.style.display = 'none'
}
