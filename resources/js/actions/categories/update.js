import links from "../../links/links";
import {loading} from '../../global/loader'
import toaster from "../../global/toaster";

$(document).ready(function () {
    $('#app').on('blur', '.content-editable-badge', function () {
        const title = $(this).text();
        const id = $(this).parent('li').attr('data-id');
        const data = {
            'title': title,
        };
        loading(true);
        axios.put(links.categories.update(id), data).then((response) => {
            if (response.status === 200) {
                toaster('Success', `${title} was successfully updated`, true);
                updateExistingCategory(id,title);
            } else {
                toaster('Warning', response.message, false);
            }
        }).catch((error) => {
            toaster('Warning', error.message, false);
            console.log(error)
        }).finally(() => {
            loading(false);
        })
    });

   function updateExistingCategory(id,text){
       $('#category_list option[value='+id+']').text(text)
    }

});
