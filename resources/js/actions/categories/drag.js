import Sortable from 'sortablejs/modular/sortable.complete.esm.js';
import {loading} from "../../global/loader";
import links from "../../links/links";
import toaster from "../../global/toaster";

const el = document.getElementById('treeMain');
var options = {
    group: 'nested',
    animation: 150,
    fallbackOnBody: true,
    swapThreshold: 0.65,
    filter: ".list-group-item-main",
    onEnd: function (evt) {
        const id = $(evt.item).attr('data-id');
        const title = $(evt.item).find('.content-editable-badge').eq(0).text();
        let parent_id = null;
        if ($(evt.to).attr('id') === 'treeMain') {
            $(evt.item).addClass('list-group-item-main');
            parent_id = 0;
        } else {
           parent_id = Number($(evt.to).parent('li').attr('data-id'));
        }
        const data = {
            title: title,
            parent_id: parent_id
        };
        console.log(data,'data');
        updateCategoryParentId(data, id);
    },
};
const nestedSortable = document.querySelectorAll('ul');
for (var i = 0; i < nestedSortable.length; i++) {
    new Sortable(nestedSortable[i], options);
}

function updateCategoryParentId(data, id) {
    loading(true);
    axios.put(links.categories.update(id), data).then((response) => {
        if (response.status === 200) {
            toaster('Success', `${data.title} was successfully updated`, true);
        } else {
            toaster('Warning', response.message, false);
        }
    }).catch((error) => {
        toaster('Warning', error.message, false);
        console.log(error)
    }).finally(() => {
        loading(false);
    })
}
