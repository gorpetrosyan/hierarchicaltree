import links from "../../links/links";
import {loading} from '../../global/loader'
import toaster from "../../global/toaster";

$(document).ready(function () {
    let globalID = null;
    $('#app').on('click','.trash-badge',function () {
        globalID = $(this).parent('li').attr('data-id');
    });

    $(document).on('click','#removeCategory',function () {
        loading(true);
        axios.delete(links.categories.remove(globalID)).then((response) => {
            if (response.status === 200) {
                removeExistingCategory(globalID);
                toaster('Success', `Category was successfully deleted`, true);
                $('#trashModal').modal('hide');
            } else {
                toaster('Warning', response.message, false);
            }
        }).catch((error) => {
            toaster('Warning', error.message, false);
            console.log(error)
        }).finally(() => {
            loading(false);
        })
    });
    function removeExistingCategory(id){
        let deletedItemsId = [];
        const elementToDelete = $('#list_'+id);
        elementToDelete.find('li').each(function (index,item) {
            deletedItemsId.push($(item).attr('data-id'))
        });
         deletedItemsId.push(id);
         removeExistingCategoryFromDropDown(deletedItemsId);
        elementToDelete.remove();
    }
    function removeExistingCategoryFromDropDown(arrayList) {
        arrayList.forEach(item => {
            $('#category_list > option[value='+item+']').remove();
        });
    }
});

