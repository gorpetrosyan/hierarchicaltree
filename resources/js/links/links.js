import {remove, update} from './categories/list'

export default {
    categories: {
        remove,
        update,
    }
};
