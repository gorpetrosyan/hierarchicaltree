export function remove(id) {
    return `/delete/${id}`
}

export function update(id) {
    return `update/${id}`
}
