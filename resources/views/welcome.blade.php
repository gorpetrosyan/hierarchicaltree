@extends('layout.app')
@section('title','Home')
@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('body')
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h3>Category Tree</h3>
                    <ul id="treeMain" class="list-group list-group-flush main-list-ul">
                        @foreach($main as $category)
                            <li class="list-group-item list-group-item-main" data-id="{{$category->id}}" id="list_{{$category->id}}">
                                <span class="badge badge-success p-2 content-editable-badge"
                                      contenteditable="true">{{ $category->title }} </span>
                                <sup data-toggle="tooltip" data-placement="top"
                                     title="Click on the category name and edit"><i class="fas fa-pen"></i></sup>
                                <a href="{{route('show',$category->id)}}" class="btn text-warning"><i class="fas fa-eye"></i></a>
                                <span class="badge badge-danger float-right trash-badge" data-toggle="modal"
                                      data-target="#trashModal"><i class="fas fa-trash"></i></span>
                                @if(count($category->subCategories))
                                    @include('layout.partials.subCategories',['subCategories' => $category->subCategories])
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-12 col-md-4">
                    <h3>Add New Category</h3>

                    <form action="{{route('store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" min="1" class="form-control" id="title" aria-describedby="titleAr"
                                   placeholder="Enter Title" name="title">
                            @if($errors->has('title'))
                                <small id="titleAr"
                                       class="form-text text-muted text-danger">{{ $errors->first('title') }}</small>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="category_list">Select Category</label>
                            <select name="parent_id" class="form-control" id="category_list"
                                    aria-describedby="parentAr">
                                @if(is_object($all) && isset($all))
                                    <option value="0" selected>Parent Category</option>
                                    @foreach($all as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('parent_id'))
                                <small id="parentAr"
                                       class="form-text text-muted text-danger">{{ $errors->first('parent_id') }}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Add new</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection

