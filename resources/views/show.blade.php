@extends('layout.app')
@section('title','Show')
@section('body')
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-12">
                    <a href="{{route('index')}}" class="btn btn-success float-right m-2">Go back</a>
                </div>
                <div class="col-12">
                    <h3>Category Tree</h3>
                    <ul id="tree1" class="list-group list-group-flush">
                        @if(isset($category) && is_object($category))
                            <li class="list-group-item" data-id="{{$category->id}}" id="list_{{$category->id}}">
                                <span class="badge badge-success p-2 content-editable-badge"
                                >{{ $category->title }} </span>
                                <a href="{{route('show',$category->id)}}" class="btn text-warning"><i class="fas fa-eye"></i></a>
                            @if(count($category->subCategories))
                                    @include('layout.partials.showList',['subCategories' => $category->subCategories])
                                @endif
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
