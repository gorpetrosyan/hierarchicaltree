<ul class="list-group">
    @foreach($subCategories as $child)
        <li class="list-group-item dragable" data-id="{{$child->id}}" id="list_{{$child->id}}">
            <span class="badge badge-info text-white p-2">{{ $child->title }}</span>
            <a href="{{route('show',$child->id)}}" class="btn text-warning"><i class="fas fa-eye"></i></a>
        @if(count($child->subCategories))
                @include('layout.partials.showList',['subCategories' => $child->subCategories])
            @endif
        </li>
    @endforeach
</ul>

