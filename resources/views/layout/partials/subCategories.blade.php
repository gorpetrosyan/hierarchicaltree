<ul class="list-group">
    @foreach($subCategories as $child)
        <li class="list-group-item dragable" data-id="{{$child->id}}" id="list_{{$child->id}}">
            <span class="badge badge-info text-white p-2 content-editable-badge" contenteditable="true">{{ $child->title }}</span>
            <sup data-toggle="tooltip" data-placement="top" title="Click on the category name and edit"><i class="fas fa-pen" ></i></sup>
            <a href="{{route('show',$child->id)}}" class="btn text-warning"><i class="fas fa-eye"></i></a>
            <span class="badge badge-danger float-right trash-badge" data-toggle="modal" data-target="#trashModal"><i class="fas fa-trash"></i></span>
            @if(count($child->subCategories))
                @include('layout.partials.subCategories',['subCategories' => $child->subCategories])
            @endif
        </li>
    @endforeach
</ul>
