    <div class="toast" id="toasterInit" style="position: absolute; top: 10px; right: 10px; background: #d2d2f1;">
        <div class="toast-header">
            <strong class="mr-auto"><i class="fas fa-info-circle"></i>  <span id="toasterHeader"></span></strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            <p id="toasterBody"></p>
        </div>
    </div>
