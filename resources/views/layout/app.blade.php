<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script>
    <title>CT | @yield('title')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
<div class="container-fluid" id="app">
    @include('layout.partials.loading')
    <div class="row">
        <div class="col-12 mt-3 mb-3">
            @include('layout.partials.notification')
        </div>
        <div class="col-12">
            @yield('body')
        </div>
    </div>
    @include('layout.partials.trashModal')
    @include('layout.partials.toaster')
</div>
<script>
    window.Laravel = {
        csrfToken : '{{csrf_token()}}'
    }
</script>
<script src="{{ asset('js/app.js') }}" defer></script>
@yield('js')
</body>
</html>
