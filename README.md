Before run the project

1. create .env file copy from .env.example and past in .env
2. create database and connect with project in .env

to run the project please run the commands

* composer install
* php artisan key:generate
* php artisan migrate
* npm install
* php artisan serve
* npm run dev
